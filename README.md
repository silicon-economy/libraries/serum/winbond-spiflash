# winbond-spiflash

Platform-specific STM32 microcontroller with QSPI support driver for the Serial NOR Flash W25Q256JV.

This library heavily relies on the hardware of the STM32 microcontroller, as there is no "trait" for the QSPI in the embedded_hal Rust Crate. Currently, only the W25Q256JV is implemented for STM32 microcontrollers with QSPI support.
## Resources

[Datasheet](https://www.winbond.com/hq/support/documentation/levelOne.jsp?__locale=en&DocNo=DA00-W25Q256JV)

[W25Q256JV at winbond.com](https://www.winbond.com/hq/product/code-storage-flash-memory/serial-nor-flash/?__locale=en&partNo=W25Q256JV)


## Usage

```rust
pub use stm32l4xx_hal as hal;

let clk = gpioa.pa3.into_alternate(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrl);
// ... pin configuration for ncs and io pins

let qspi_config = hal::qspi::QspiConfig::default()
    .clock_prescaler(0)
    .flash_size(24)
    .address_size(hal::qspi::AddressSize::Addr24Bit);

let qspi = hal::qspi::Qspi::new(
    dp.QUADSPI,
    (clk, ncs, io_0, io_1, io_2, io_3),
    &mut rcc.ahb3,
    qspi_config,
);

let mut w25qx = W25qx::new(qspi, ByteMode::Addr24Bit);
let mut data: [u8; 9000] = [0; 9000];
for i in 0..data.len() {
    data[i] = 1;
}
let mut buf: [u8; 4096*3] = [0; 4096*3];
w25qx.write(0x00000000, &data).map_err(|_| "Could not write data at addresse 0x00000000");
w25qx.read(0x00000000, &mut buf).map_err(|_| "Could not read data at addresse 0x00000000");
```

## Features
- `standalone`: This feature selects one of the `stm32l4xx-hal` chips. This is required if the library shall be built standalone (e.g. for testing). If it is used as a dependency, the application itself will probably already have selected one of the `stm32l4xx-hal` features.

## License

Licensed under the Open Logistics Foundation License 1.3.

## Contact

Fraunhofer IML Embedded Rust Group - embedded-rust@iml.fraunhofer.de

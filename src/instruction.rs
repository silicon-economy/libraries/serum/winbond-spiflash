// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

pub enum ReadInstruction {
    Read3ByteMode = 0x03,
    Read4ByteMode = 0x13,
    FastRead3ByteMode = 0x0b,
    FastRead4ByteMode = 0x0c,
    FastReadDual3ByteMode = 0x3b,
    FastReadDual4ByteMode = 0x3c,
    FastReadQuad3ByteMode = 0x6b,
    FastReadQuad4ByteMode = 0x6c,
    FastReadDualIO3ByteMode = 0xbb,
    FastReadDualIO4ByteMode = 0xbc,
    FastReadQuadIO3ByteMode = 0xeb,
    FastReadQuadIO4ByteMode = 0xec,
    DeviceID = 0xab,
    ReadManufacturesDeviceID = 0x90,
    ReadManufacturesDeviceIDDualIO = 0x92,
    ReadManufacturesDeviceIDQuadIO = 0x94,
    ReadUniqueID = 0x4b,
    ReadJedecID = 0x9f,
    ReadLock = 0x3d,
}

pub enum WriteInstruction {
    WriteDisable = 0x04,
    WriteEnable = 0x06,
    PageProgram4ByteMode = 0x12,
    PageProgram3ByteMode = 0x02,
    QuadInputPageProgram3ByteMode = 0x32,
    QuadInputPageProgram4ByteMode = 0x34,
    SectorErase4ByteMode = 0x21,
    SectorErase3ByteMode = 0x20,
    BlockErase32KB3ByteMode = 0x52,
    BlockErase64KB3ByteMode = 0xd8,
    BlockErase64KB4ByteMode = 0xdc,
    ChipErase = 0x60, // or 0xc7
    EraseProgramSuspend = 0x75,
    EraseProgramResume = 0x7a,
    PowerDown = 0xb9,
    ReleasePowerDown = 0xab,
    SetBurstWithWrap = 0x77,
    IndividualLock = 0x36,
    IndividualUnlock = 0x39,
    GlobalLock = 0x7e,
    GlobalUnlock = 0x98,
    EnableReset = 0x66,
    ResetDevice = 0x99,
}

pub enum RegisterInstruction {
    WriteEnableStatusRegister = 0x50,
    ReadStatusRegister1 = 0x05,
    ReadStatusRegister2 = 0x35,
    ReadStatusRegister3 = 0x15,
    WriteStatusRegister1 = 0x01,
    WriteStatusRegister2 = 0x31,
    WriteStatusRegister3 = 0x11,
    ReadExtendedAdressRegister = 0xc8,
    WriteExtendedAdressRegister = 0xc5,
    Enter4ByteAddressMode = 0xb7,
    Exit4ByteAdressMode = 0xe9,
    ReadSFDPRegister = 0x5a,
    EraseSecurityRegister = 0x44,
    ProgramSecurityRegister = 0x42,
    ReadSecurityRegister = 0x48,
}

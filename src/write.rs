// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use crate::instruction::{RegisterInstruction, WriteInstruction};
use crate::{w25qx::SecurityRegister3ByteAddress, ByteMode, W25qx};
use hal::{
    qspi::{ClkPin, IO0Pin, IO1Pin, IO2Pin, IO3Pin, NCSPin, QspiError},
    stm32::QUADSPI,
};
pub use stm32l4xx_hal as hal;

/// These functions execute low level hardware write instructions.
/// For more information, please refer to the datasheet that explains the instructions.
impl<CLK, NCS, IO0, IO1, IO2, IO3> W25qx<(CLK, NCS, IO0, IO1, IO2, IO3)>
where
    CLK: ClkPin<QUADSPI>,
    NCS: NCSPin<QUADSPI>,
    IO0: IO0Pin<QUADSPI>,
    IO1: IO1Pin<QUADSPI>,
    IO2: IO2Pin<QUADSPI>,
    IO3: IO3Pin<QUADSPI>,
{
    pub fn write_enable(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::WriteEnable as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn write_enable_status_register(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                RegisterInstruction::WriteEnableStatusRegister as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn write_disable(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::WriteDisable as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn write_status_register(
        &mut self,
        data: &[u8],
        register: RegisterInstruction,
    ) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((register as u8, hal::qspi::QspiMode::SingleChannel)),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: Some((data, hal::qspi::QspiMode::SingleChannel)),
            double_data_rate: false,
        };
        match self.qspi.write(write_command) {
            Ok(_) => while self.is_busy() {},
            Err(err) => return Err(err),
        };
        Ok(())
    }

    pub fn write_extended_address_register(&mut self, data: &[u8]) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                RegisterInstruction::WriteExtendedAdressRegister as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: Some((data, hal::qspi::QspiMode::SingleChannel)),
            double_data_rate: false,
        };
        match self.qspi.write(write_command) {
            Ok(_) => while self.is_busy() {},
            Err(err) => return Err(err),
        };
        Ok(())
    }

    pub fn enter_4_byte_address_mode(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                RegisterInstruction::Enter4ByteAddressMode as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        match self.qspi.write(write_command) {
            Ok(_) => while self.is_busy() {},
            Err(err) => return Err(err),
        };
        Ok(())
    }

    pub fn exit_4_byte_address_mode(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                RegisterInstruction::Exit4ByteAdressMode as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn page_program(&mut self, address: u32, data: &[u8]) -> Result<(), QspiError> {
        let instruction = match self.byte_mode {
            ByteMode::Addr24Bit => WriteInstruction::PageProgram3ByteMode as u8,
            ByteMode::Addr32Bit => WriteInstruction::PageProgram4ByteMode as u8,
        };
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((instruction, hal::qspi::QspiMode::SingleChannel)),
            address: Some((address, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 0,
            data: Some((data, hal::qspi::QspiMode::SingleChannel)),
            double_data_rate: false,
        };
        match self.qspi.write(write_command) {
            Ok(_) => while self.is_busy() {},
            Err(err) => return Err(err),
        };
        Ok(())
    }

    pub fn sector_erase(&mut self, address: u32) -> Result<(), QspiError> {
        let instruction = match self.byte_mode {
            ByteMode::Addr24Bit => WriteInstruction::SectorErase3ByteMode as u8,
            ByteMode::Addr32Bit => WriteInstruction::SectorErase4ByteMode as u8,
        };
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((instruction, hal::qspi::QspiMode::SingleChannel)),
            address: Some((address, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        match self.qspi.write(write_command) {
            Ok(_) => while self.is_busy() {},
            Err(err) => return Err(err),
        };
        Ok(())
    }

    pub fn block32kb_erase(&mut self, address: u32) -> Result<(), QspiError> {
        let instruction = match self.byte_mode {
            ByteMode::Addr24Bit => WriteInstruction::BlockErase32KB3ByteMode as u8,
            ByteMode::Addr32Bit => return Err(QspiError::Address),
        };

        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((instruction, hal::qspi::QspiMode::SingleChannel)),
            address: Some((address, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        match self.qspi.write(write_command) {
            Ok(_) => while self.is_busy() {},
            Err(err) => return Err(err),
        };
        Ok(())
    }

    pub fn block64kb_erase(&mut self, address: u32) -> Result<(), QspiError> {
        let instruction = match self.byte_mode {
            ByteMode::Addr24Bit => WriteInstruction::BlockErase64KB3ByteMode as u8,
            ByteMode::Addr32Bit => WriteInstruction::BlockErase64KB4ByteMode as u8,
        };
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((instruction, hal::qspi::QspiMode::SingleChannel)),
            address: Some((address, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        match self.qspi.write(write_command) {
            Ok(_) => while self.is_busy() {},
            Err(err) => return Err(err),
        };
        Ok(())
    }

    pub fn lock(&mut self, address: u32) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::IndividualLock as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: Some((address, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn unlock(&mut self, address: u32) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::IndividualUnlock as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: Some((address, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn global_unlock(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::GlobalUnlock as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn global_lock(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::GlobalLock as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn erase_program_resume(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::EraseProgramResume as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn erase_program_suspend(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::EraseProgramSuspend as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn power_down(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::PowerDown as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn release_power_down(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::ReleasePowerDown as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn erase_security_register(
        &mut self,
        register: SecurityRegister3ByteAddress,
    ) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                RegisterInstruction::EraseSecurityRegister as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: Some((register as u32, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn program_security_register(
        &mut self,
        data: &[u8],
        register: SecurityRegister3ByteAddress,
    ) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                RegisterInstruction::ProgramSecurityRegister as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: Some((register as u32, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 0,
            data: Some((data, hal::qspi::QspiMode::SingleChannel)),
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn enable_reset(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::EnableReset as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        match self.qspi.write(write_command) {
            Ok(_) => while self.is_busy() {},
            Err(err) => return Err(err),
        };
        Ok(())
    }
    pub fn reset_device(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::ResetDevice as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        self.qspi.write(write_command)
    }

    pub fn chip_erase(&mut self) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::ChipErase as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        match self.qspi.write(write_command) {
            Ok(_) => while self.is_busy() {},
            Err(err) => return Err(err),
        };
        Ok(())
    }

    pub fn quad_input_page_program(
        &mut self,
        address: u32,
        data: &[u8],
        byte_mode: ByteMode,
    ) -> Result<(), QspiError> {
        let instruction = match byte_mode {
            ByteMode::Addr24Bit => WriteInstruction::QuadInputPageProgram3ByteMode as u8,
            ByteMode::Addr32Bit => WriteInstruction::QuadInputPageProgram4ByteMode as u8,
        };
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((instruction, hal::qspi::QspiMode::SingleChannel)),
            address: Some((address, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 0,
            data: Some((data, hal::qspi::QspiMode::QuadChannel)),
            double_data_rate: false,
        };
        match self.qspi.write(write_command) {
            Ok(_) => while self.is_busy() {},
            Err(err) => return Err(err),
        };
        Ok(())
    }

    pub fn set_burst_with_wrap(&mut self, wrap: &[u8]) -> Result<(), QspiError> {
        let write_command = hal::qspi::QspiWriteCommand {
            instruction: Some((
                WriteInstruction::SetBurstWithWrap as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: Some((wrap, hal::qspi::QspiMode::QuadChannel)),
            dummy_cycles: 0,
            data: None,
            double_data_rate: false,
        };
        match self.qspi.write(write_command) {
            Ok(_) => while self.is_busy() {},
            Err(err) => return Err(err),
        };
        Ok(())
    }
}

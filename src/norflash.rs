// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use crate::W25qx;
use embedded_storage::nor_flash::{
    check_erase, check_read, check_write, ErrorType, NorFlash, NorFlashErrorKind, ReadNorFlash,
};
use hal::{
    qspi::{ClkPin, IO0Pin, IO1Pin, IO2Pin, IO3Pin, NCSPin},
    stm32::QUADSPI,
};
pub use stm32l4xx_hal as hal;

impl<CLK, NCS, IO0, IO1, IO2, IO3> ErrorType for W25qx<(CLK, NCS, IO0, IO1, IO2, IO3)>
where
    CLK: ClkPin<QUADSPI>,
    NCS: NCSPin<QUADSPI>,
    IO0: IO0Pin<QUADSPI>,
    IO1: IO1Pin<QUADSPI>,
    IO2: IO2Pin<QUADSPI>,
    IO3: IO3Pin<QUADSPI>,
{
    type Error = NorFlashErrorKind;
}

impl<CLK, NCS, IO0, IO1, IO2, IO3> ReadNorFlash for W25qx<(CLK, NCS, IO0, IO1, IO2, IO3)>
where
    CLK: ClkPin<QUADSPI>,
    NCS: NCSPin<QUADSPI>,
    IO0: IO0Pin<QUADSPI>,
    IO1: IO1Pin<QUADSPI>,
    IO2: IO2Pin<QUADSPI>,
    IO3: IO3Pin<QUADSPI>,
{
    const READ_SIZE: usize = 1;

    fn capacity(&self) -> usize {
        0x01ffffff
    }

    fn read(&mut self, offset: u32, bytes: &mut [u8]) -> Result<(), Self::Error> {
        check_read(self, offset, bytes.len())?;
        self.fast_read_quad_output(offset, bytes)
            .map_err(|_| NorFlashErrorKind::Other)
    }
}

impl<CLK, NCS, IO0, IO1, IO2, IO3> NorFlash for W25qx<(CLK, NCS, IO0, IO1, IO2, IO3)>
where
    CLK: ClkPin<QUADSPI>,
    NCS: NCSPin<QUADSPI>,
    IO0: IO0Pin<QUADSPI>,
    IO1: IO1Pin<QUADSPI>,
    IO2: IO2Pin<QUADSPI>,
    IO3: IO3Pin<QUADSPI>,
{
    const ERASE_SIZE: usize = 4096;
    const WRITE_SIZE: usize = 1;

    fn erase(&mut self, from: u32, to: u32) -> Result<(), Self::Error> {
        check_erase(self, from, to)?;
        self.check_3_byte_mode(from)
            .map_err(|_| NorFlashErrorKind::Other)?;

        const SECTOR_SIZE: u32 = 4096;
        const HALF_BLOCK_SIZE: u32 = SECTOR_SIZE * 8;
        const BLOCK_SIZE: u32 = SECTOR_SIZE * 16;

        let erase_size = to - from;

        if erase_size == SECTOR_SIZE {
            self.sector_erase(from)
                .map_err(|_| NorFlashErrorKind::Other)
        } else if erase_size == HALF_BLOCK_SIZE {
            self.block32kb_erase(from)
                .map_err(|_| NorFlashErrorKind::Other)
        } else if erase_size == BLOCK_SIZE {
            self.block64kb_erase(from)
                .map_err(|_| NorFlashErrorKind::Other)
        } else if erase_size == self.capacity() as u32 {
            self.chip_erase().map_err(|_| NorFlashErrorKind::Other)
        } else {
            return Err(NorFlashErrorKind::Other);
        }
    }

    fn write(&mut self, offset: u32, bytes: &[u8]) -> Result<(), Self::Error> {
        check_write(self, offset, bytes.len())?;
        self.check_3_byte_mode(offset)
            .map_err(|_| NorFlashErrorKind::Other)?;
        const PAGE_SIZE: usize = 256;
        if bytes.len() > PAGE_SIZE {
            Err(NorFlashErrorKind::Other)
        } else {
            self.page_program(offset, bytes)
                .map_err(|_| NorFlashErrorKind::Other)
        }
    }
}

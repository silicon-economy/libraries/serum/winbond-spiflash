// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use crate::instruction::{ReadInstruction, RegisterInstruction};
use crate::{w25qx::SecurityRegister3ByteAddress, ByteMode, W25qx};
use hal::{
    qspi::{ClkPin, IO0Pin, IO1Pin, IO2Pin, IO3Pin, NCSPin, QspiError},
    stm32::QUADSPI,
};
pub use stm32l4xx_hal as hal;

/// These functions execute low level hardware read instructions.
/// For more information, please refer to the datasheet that explains the instructions.
impl<CLK, NCS, IO0, IO1, IO2, IO3> W25qx<(CLK, NCS, IO0, IO1, IO2, IO3)>
where
    CLK: ClkPin<QUADSPI>,
    NCS: NCSPin<QUADSPI>,
    IO0: IO0Pin<QUADSPI>,
    IO1: IO1Pin<QUADSPI>,
    IO2: IO2Pin<QUADSPI>,
    IO3: IO3Pin<QUADSPI>,
{
    pub fn read_status_register(
        &mut self,
        buf: &mut [u8],
        register: RegisterInstruction,
    ) -> Result<(), QspiError> {
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((register as u8, hal::qspi::QspiMode::SingleChannel)),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data_mode: hal::qspi::QspiMode::SingleChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn read_sfdp_register(&mut self, buf: &mut [u8]) -> Result<(), QspiError> {
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((
                RegisterInstruction::ReadSFDPRegister as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: Some((0x000000, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 7,
            data_mode: hal::qspi::QspiMode::SingleChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn read_jedec_id(&mut self, buf: &mut [u8]) -> Result<(), QspiError> {
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((
                ReadInstruction::ReadJedecID as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data_mode: hal::qspi::QspiMode::SingleChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn read_unique_id(&mut self, buf: &mut [u8]) -> Result<(), QspiError> {
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((
                ReadInstruction::ReadUniqueID as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 31,
            data_mode: hal::qspi::QspiMode::SingleChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn read_device_id(&mut self, buf: &mut [u8]) -> Result<(), QspiError> {
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((
                ReadInstruction::DeviceID as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 23,
            data_mode: hal::qspi::QspiMode::SingleChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn read_manufacturer_device_id(&mut self, buf: &mut [u8]) -> Result<(), QspiError> {
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((
                ReadInstruction::ReadManufacturesDeviceID as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: Some((0x000000, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 0,
            data_mode: hal::qspi::QspiMode::SingleChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn read_extended_address_register(&mut self, buf: &mut [u8]) -> Result<(), QspiError> {
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((
                RegisterInstruction::ReadExtendedAdressRegister as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: None,
            alternative_bytes: None,
            dummy_cycles: 0,
            data_mode: hal::qspi::QspiMode::SingleChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn read_data(&mut self, address: u32, buf: &mut [u8]) -> Result<(), QspiError> {
        let instruction = match self.byte_mode {
            ByteMode::Addr24Bit => ReadInstruction::Read3ByteMode as u8,
            ByteMode::Addr32Bit => ReadInstruction::Read4ByteMode as u8,
        };
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((instruction, hal::qspi::QspiMode::SingleChannel)),
            address: Some((address, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 0,
            data_mode: hal::qspi::QspiMode::SingleChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn fast_read(&mut self, address: u32, buf: &mut [u8]) -> Result<(), QspiError> {
        let instruction = match self.byte_mode {
            ByteMode::Addr24Bit => ReadInstruction::FastRead3ByteMode as u8,
            ByteMode::Addr32Bit => ReadInstruction::FastRead4ByteMode as u8,
        };
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((instruction, hal::qspi::QspiMode::SingleChannel)),
            address: Some((address, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 8,
            data_mode: hal::qspi::QspiMode::SingleChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn fast_read_dual_output(&mut self, address: u32, buf: &mut [u8]) -> Result<(), QspiError> {
        let instruction = match self.byte_mode {
            ByteMode::Addr24Bit => ReadInstruction::FastReadDual3ByteMode as u8,
            ByteMode::Addr32Bit => ReadInstruction::FastReadDual4ByteMode as u8,
        };
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((instruction, hal::qspi::QspiMode::SingleChannel)),
            address: Some((address, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 8,
            data_mode: hal::qspi::QspiMode::DualChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn fast_read_quad_output(&mut self, address: u32, buf: &mut [u8]) -> Result<(), QspiError> {
        let instruction = match self.byte_mode {
            ByteMode::Addr24Bit => ReadInstruction::FastReadQuad3ByteMode as u8,
            ByteMode::Addr32Bit => ReadInstruction::FastReadQuad4ByteMode as u8,
        };
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((instruction, hal::qspi::QspiMode::SingleChannel)),
            address: Some((address, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 8,
            data_mode: hal::qspi::QspiMode::QuadChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn fast_read_dual_input_output(
        &mut self,
        address: u32,
        buf: &mut [u8],
    ) -> Result<(), QspiError> {
        let instruction = match self.byte_mode {
            ByteMode::Addr24Bit => ReadInstruction::FastReadDualIO3ByteMode as u8,
            ByteMode::Addr32Bit => ReadInstruction::FastReadDualIO4ByteMode as u8,
        };
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((instruction, hal::qspi::QspiMode::SingleChannel)),
            address: Some((address, hal::qspi::QspiMode::DualChannel)),
            alternative_bytes: Some((&[0xff], hal::qspi::QspiMode::DualChannel)),
            dummy_cycles: 0,
            data_mode: hal::qspi::QspiMode::DualChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn fast_read_quad_input_output(
        &mut self,
        address: u32,
        buf: &mut [u8],
    ) -> Result<(), QspiError> {
        let instruction = match self.byte_mode {
            ByteMode::Addr24Bit => ReadInstruction::FastReadQuadIO3ByteMode as u8,
            ByteMode::Addr32Bit => ReadInstruction::FastReadQuadIO4ByteMode as u8,
        };
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((instruction, hal::qspi::QspiMode::SingleChannel)),
            address: Some((address, hal::qspi::QspiMode::QuadChannel)),
            alternative_bytes: Some((&[0xff], hal::qspi::QspiMode::QuadChannel)),
            dummy_cycles: 4,
            data_mode: hal::qspi::QspiMode::QuadChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn read_lock(&mut self, address: u32, buf: &mut [u8]) -> Result<(), QspiError> {
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((
                ReadInstruction::ReadLock as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: Some((address, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 0,
            data_mode: hal::qspi::QspiMode::SingleChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn read_security_register(
        &mut self,
        buf: &mut [u8],
        register: SecurityRegister3ByteAddress,
    ) -> Result<(), QspiError> {
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((
                RegisterInstruction::ReadSecurityRegister as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: Some((register as u32, hal::qspi::QspiMode::SingleChannel)),
            alternative_bytes: None,
            dummy_cycles: 0,
            data_mode: hal::qspi::QspiMode::SingleChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn read_manufacturer_device_id_dual_input_output(
        &mut self,
        buf: &mut [u8],
    ) -> Result<(), QspiError> {
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((
                ReadInstruction::ReadManufacturesDeviceIDDualIO as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: Some((0x000000, hal::qspi::QspiMode::DualChannel)),
            alternative_bytes: Some((&[0xff], hal::qspi::QspiMode::DualChannel)),
            dummy_cycles: 0,
            data_mode: hal::qspi::QspiMode::DualChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }

    pub fn read_manufacturer_device_id_quad_input_output(
        &mut self,
        buf: &mut [u8],
    ) -> Result<(), QspiError> {
        let read_command = hal::qspi::QspiReadCommand {
            instruction: Some((
                ReadInstruction::ReadManufacturesDeviceIDQuadIO as u8,
                hal::qspi::QspiMode::SingleChannel,
            )),
            address: Some((0x000000, hal::qspi::QspiMode::QuadChannel)),
            alternative_bytes: Some((&[0xf0, 1], hal::qspi::QspiMode::QuadChannel)),
            dummy_cycles: 4,
            data_mode: hal::qspi::QspiMode::QuadChannel,
            receive_length: buf.len() as u32,
            double_data_rate: false,
        };
        self.qspi.transfer(read_command, buf)
    }
}

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use crate::{Error, W25qx};
use embedded_storage::{ReadStorage, Storage};
pub use stm32l4xx_hal as hal;

use hal::{
    qspi::{ClkPin, IO0Pin, IO1Pin, IO2Pin, IO3Pin, NCSPin, QspiError},
    stm32::QUADSPI,
};

const SECTOR_SIZE: usize = 4096;

impl<CLK, NCS, IO0, IO1, IO2, IO3> W25qx<(CLK, NCS, IO0, IO1, IO2, IO3)>
where
    CLK: ClkPin<QUADSPI>,
    NCS: NCSPin<QUADSPI>,
    IO0: IO0Pin<QUADSPI>,
    IO1: IO1Pin<QUADSPI>,
    IO2: IO2Pin<QUADSPI>,
    IO3: IO3Pin<QUADSPI>,
{
    // Write a 4KB sector to the NOR flash.
    fn write_sector(&mut self, address: u32, data: &[u8]) -> Result<(), QspiError> {
        let mut start = address;
        for chunk in data.chunks(256) {
            self.write_enable()?;
            self.page_program(start, chunk)?;
            start += chunk.len() as u32;
        }

        Ok(())
    }
}

impl<CLK, NCS, IO0, IO1, IO2, IO3> ReadStorage for W25qx<(CLK, NCS, IO0, IO1, IO2, IO3)>
where
    CLK: ClkPin<QUADSPI>,
    NCS: NCSPin<QUADSPI>,
    IO0: IO0Pin<QUADSPI>,
    IO1: IO1Pin<QUADSPI>,
    IO2: IO2Pin<QUADSPI>,
    IO3: IO3Pin<QUADSPI>,
{
    type Error = Error<QspiError>;

    // This function allows one or more data bytes to be sequentially read from the memory.
    // Fast Read Quad I/O' instruction of the NOR flash is used, which utilizes 4 channels for reading data from the memory.
    fn read(&mut self, offset: u32, bytes: &mut [u8]) -> Result<(), Self::Error> {
        self.check_3_byte_mode(offset)?;
        if offset as usize > self.capacity() {
            return Err(Error::InvalidAddr);
        }
        self.fast_read_quad_output(offset, bytes)
            .map_err(Error::Qspi)
    }

    // Return the maximum capacity of the NOR Flash.
    fn capacity(&self) -> usize {
        0x02000000
    }
}

impl<CLK, NCS, IO0, IO1, IO2, IO3> Storage for W25qx<(CLK, NCS, IO0, IO1, IO2, IO3)>
where
    CLK: ClkPin<QUADSPI>,
    NCS: NCSPin<QUADSPI>,
    IO0: IO0Pin<QUADSPI>,
    IO1: IO1Pin<QUADSPI>,
    IO2: IO2Pin<QUADSPI>,
    IO3: IO3Pin<QUADSPI>,
{
    // Write data bytes to the NOR flash.
    fn write(&mut self, offset: u32, bytes: &[u8]) -> Result<(), Self::Error> {
        let sector_write_jobs =
            SectorWriteJobIterator::try_new(self.capacity(), offset, bytes.len())?;

        // Read the sector and store its contents in a buffer.
        // Next, replace the elements in the buffer with the corresponding elements from the data that needs to be written to the NOR flash.
        // Once the buffer is updated, proceed to erase the sector and write the modified buffer contents to the NOR Flash.
        for job in sector_write_jobs {
            self.check_3_byte_mode(job.start_address)?;
            if job.len == SECTOR_SIZE {
                self.write_enable().map_err(Error::Qspi)?;
                self.sector_erase(job.start_address).map_err(Error::Qspi)?;
                self.write_sector(
                    job.start_address,
                    &bytes[job.bytes_start..job.bytes_start + job.len],
                )
                .map_err(Error::Qspi)?;
            } else {
                let mut sector: [u8; SECTOR_SIZE] = [0; SECTOR_SIZE];
                self.read(job.start_address, &mut sector).unwrap();
                sector[job.sector_start..job.sector_start + job.len]
                    .copy_from_slice(&bytes[job.bytes_start..job.bytes_start + job.len]);
                self.write_enable().map_err(Error::Qspi)?;
                self.sector_erase(job.start_address).map_err(Error::Qspi)?;
                self.write_sector(job.start_address, &sector)
                    .map_err(Error::Qspi)?;
            }
        }

        Ok(())
    }
}

#[derive(Debug)]
struct SectorWriteJob {
    /// Start address of the sector in which the write happens, multiples of SECTOR_SIZE
    start_address: u32,
    /// Offset in the sector where the write starts, sector_start = write_address - start_address
    sector_start: usize,
    /// Offset in the byte array where this write starts
    bytes_start: usize,
    /// Length in bytes of this write job
    len: usize,
}

#[derive(Debug)]
struct SectorWriteJobIterator {
    /// Start address of the full write operation
    write_address: u32,
    bytes_written: usize,
    bytes_total: usize,
}

impl SectorWriteJobIterator {
    fn try_new<QspiErr>(
        capacity: usize,
        offset: u32,
        bytes_len: usize,
    ) -> Result<Self, Error<QspiErr>> {
        if offset as usize > capacity {
            return Err(Error::InvalidAddr);
        }
        if (offset as usize + bytes_len) > capacity {
            return Err(Error::TooMuchData);
        }

        Ok(Self {
            write_address: offset,
            bytes_written: 0,
            bytes_total: bytes_len,
        })
    }
}

impl Iterator for SectorWriteJobIterator {
    type Item = SectorWriteJob;
    fn next(&mut self) -> Option<<Self as Iterator>::Item> {
        if self.bytes_written == self.bytes_total {
            return None;
        }
        let job_write_address = self.write_address + self.bytes_written as u32;
        let start_address = job_write_address & 0xfffff000;
        let sector_start = (job_write_address - start_address) as usize;
        let sector_remaining = SECTOR_SIZE - sector_start;
        let bytes_remaining = self.bytes_total - self.bytes_written;
        let len = core::cmp::min(sector_remaining, bytes_remaining);
        let bytes_start = self.bytes_written;
        self.bytes_written += len;
        Some(SectorWriteJob {
            start_address,
            sector_start,
            bytes_start,
            len,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::SectorWriteJobIterator;
    use super::SECTOR_SIZE;

    #[test]
    fn write_jobs_1() {
        let bytes_len = 1;
        let capacity = 0x20000000;
        let offset = 0x00000000;
        let jobs = SectorWriteJobIterator::try_new::<()>(capacity, offset, bytes_len).unwrap();
        let job = jobs.last().unwrap();

        assert_eq!(job.start_address, 0);
        assert_eq!(job.bytes_start, 0);
        assert_eq!(job.sector_start, 0);
        assert_eq!(job.len, 1);
    }

    #[test]
    fn write_jobs_2() {
        let bytes_len = 4096;
        let capacity = 0x20000000;
        let offset = 0x000017D0;
        let jobs = SectorWriteJobIterator::try_new::<()>(capacity, offset, bytes_len).unwrap();
        let job = jobs.last().unwrap();

        let last_address = offset + bytes_len as u32;
        let last_start_address = last_address & 0xfffff000;
        let last_byte_start = bytes_len - (last_address - last_start_address) as usize;

        assert_eq!(job.bytes_start, last_byte_start);
        assert_eq!(job.start_address, last_start_address);
        assert_eq!(job.sector_start, 0);
        assert_eq!(job.len, 0x7d0);
    }

    #[test]
    fn write_jobs_3() {
        let bytes_len = 4096;
        let capacity = 0x20000000;
        let offset = 0x00002000;
        let jobs = SectorWriteJobIterator::try_new::<()>(capacity, offset, bytes_len).unwrap();
        let job = jobs.last().unwrap();

        let last_start_address = 0x2000;
        let last_byte_start = 0;

        assert_eq!(job.bytes_start, last_byte_start);
        assert_eq!(job.start_address, last_start_address);
        assert_eq!(job.sector_start, 0);
        assert_eq!(job.len, 4096);
    }

    #[test]
    fn write_jobs_4() {
        let bytes_len = 2000;
        let capacity = 0x20000000;
        let offset = 0x00003830;
        let jobs = SectorWriteJobIterator::try_new::<()>(capacity, offset, bytes_len).unwrap();
        let job = jobs.last().unwrap();

        let last_start_address = offset & 0xfffff000;
        let last_byte_start = 0;

        assert_eq!(job.bytes_start, last_byte_start);
        assert_eq!(job.start_address, last_start_address);
        assert_eq!(job.sector_start, 0x830);
        assert_eq!(job.len, 2000);
    }

    #[test]
    fn write_jobs_5() {
        let bytes_len = 8000;
        let capacity = 0x20000000;
        let offset = 0x00004830;
        let jobs = SectorWriteJobIterator::try_new::<()>(capacity, offset, bytes_len).unwrap();
        let job = jobs.last().unwrap();

        let last_address = offset + bytes_len as u32;
        let last_start_address = last_address & 0xfffff000;
        let last_byte_start = bytes_len - (last_address - last_start_address) as usize;

        assert_eq!(job.bytes_start, last_byte_start);
        assert_eq!(job.start_address, last_start_address);
        assert_eq!(job.sector_start, 0);
        assert_eq!(job.len, 8000 - 4096 + 0x830 - 4096);
    }

    #[test]
    fn write_jobs_6() {
        let bytes_len = 9000;
        let capacity = 0x20000000;
        let offset = 0x00008000;
        let jobs = SectorWriteJobIterator::try_new::<()>(capacity, offset, bytes_len).unwrap();
        let job = jobs.last().unwrap();

        let last_address = offset + bytes_len as u32;
        let last_start_address = last_address & 0xfffff000;
        let last_byte_start = bytes_len - (last_address - last_start_address) as usize;

        assert_eq!(job.bytes_start, last_byte_start);
        assert_eq!(job.start_address, last_start_address);
        assert_eq!(job.sector_start, 0);
        assert_eq!(job.len, 9000 - 2 * 4096);
    }

    #[test]
    fn write_jobs_7() {
        let bytes_len = 9000;
        let capacity = 0x20000000;
        let offset = 0x00040CD8;
        let jobs = SectorWriteJobIterator::try_new::<()>(capacity, offset, bytes_len).unwrap();
        let job = jobs.last().unwrap();

        let last_address = offset + bytes_len as u32 - 1;
        let last_start_address = last_address & 0xfffff000;
        let last_byte_start = bytes_len - SECTOR_SIZE;

        assert_eq!(job.bytes_start, last_byte_start);
        assert_eq!(job.start_address, last_start_address);
        assert_eq!(job.sector_start, 0);
        assert_eq!(job.len, 4096);
    }
}

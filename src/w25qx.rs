// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use crate::instruction::RegisterInstruction;
use crate::{ByteMode, Error, W25qx};
use hal::{
    qspi::{ClkPin, IO0Pin, IO1Pin, IO2Pin, IO3Pin, NCSPin, QspiError},
    stm32::QUADSPI,
};
pub use stm32l4xx_hal as hal;

pub enum SecurityRegister3ByteAddress {
    Register1 = 0x00100,
    Register2 = 0x00200,
    Register3 = 0x00300,
}

impl<CLK, NCS, IO0, IO1, IO2, IO3> W25qx<(CLK, NCS, IO0, IO1, IO2, IO3)>
where
    CLK: ClkPin<QUADSPI>,
    NCS: NCSPin<QUADSPI>,
    IO0: IO0Pin<QUADSPI>,
    IO1: IO1Pin<QUADSPI>,
    IO2: IO2Pin<QUADSPI>,
    IO3: IO3Pin<QUADSPI>,
{
    /// Creates a new `W25qx` struct, requires the qspi hardware peripheral and the desired `ByteMode` that shall be used
    ///
    /// Afterwards, `setup` must be called.
    pub fn new(qspi: hal::qspi::Qspi<(CLK, NCS, IO0, IO1, IO2, IO3)>, byte_mode: ByteMode) -> Self {
        W25qx { qspi, byte_mode }
    }

    /// This function is used to set the flash memory into 4-byte mode or exit
    /// the 4-byte mode, depending on the specified address bits (24 or 32).
    pub fn setup(&mut self) -> Result<(), Error<QspiError>> {
        self.write_enable().map_err(Error::Qspi)?;
        match self.byte_mode {
            ByteMode::Addr24Bit => self.exit_4_byte_address_mode().map_err(Error::Qspi),
            ByteMode::Addr32Bit => self.enter_4_byte_address_mode().map_err(Error::Qspi),
        }
    }

    /// Checks if a specific bit in a given byte is set (equals 1).
    fn is_bit_set(&mut self, byte: u8, n: u8) -> bool {
        (byte & (1 << n)) > 0
    }

    /// The function checks if the flash memory device is currently busy executing certain
    /// operations, such as Page Program, Quad Page Program, Sector Erase, Block Erase, Chip Erase,
    /// Write Status Register, or Erase/Program Security Register. The device sets a "Busy" bit in
    /// its Status Register 1.
    pub fn is_busy(&mut self) -> bool {
        let mut buf: [u8; 1] = [0; 1];
        self.read_status_register(&mut buf, RegisterInstruction::ReadStatusRegister1)
            .unwrap();
        self.is_bit_set(buf[0], 0)
    }

    /// The Suspend Status bit is a read only bit in the status register 2 that is set to 1 after executing a
    /// Erase/Program Suspend instruction. The SUS status bit is cleared to 0 by Erase/Program Resume
    /// instruction as well as a power-down, power-up cycle.
    pub fn is_sus(&mut self) -> bool {
        let mut buf: [u8; 1] = [0; 1];
        self.read_status_register(&mut buf, RegisterInstruction::ReadStatusRegister2)
            .unwrap();
        self.is_bit_set(buf[0], 7)
    }

    /// The WPS bit selects the Write Protect scheme.
    /// - When WPS=0, CMP, TB, BP\[3:0\] bits protect memory areas.
    /// - When WPS=1, Individual Block Locks protect sectors or blocks.
    /// - Default: All Individual Block Lock bits are 1 on device power-on or reset
    pub fn is_wps(&mut self) -> bool {
        let mut buf: [u8; 1] = [0; 1];
        self.read_status_register(&mut buf, RegisterInstruction::ReadStatusRegister3)
            .unwrap();
        self.is_bit_set(buf[0], 2)
    }

    /// Set wps bit to 1 in status register 3
    pub fn set_wps_bit(&mut self) {
        if !self.is_wps() {
            let mut buf: [u8; 1] = [0; 1];
            self.read_status_register(&mut buf, RegisterInstruction::ReadStatusRegister3)
                .unwrap();
            let byte_wps_set = buf[0] | (1 << 2);
            self.write_enable().unwrap();
            self.write_enable_status_register().unwrap();
            self.write_status_register(&[byte_wps_set], RegisterInstruction::WriteStatusRegister3)
                .unwrap();
            while self.is_busy() {}
        }
    }

    /// Set wps bit to 0 in status register 3
    pub fn clear_wps_bit(&mut self) {
        if self.is_wps() {
            let mut buf: [u8; 1] = [0; 1];
            self.read_status_register(&mut buf, RegisterInstruction::ReadStatusRegister3)
                .unwrap();
            let byte_wps_clear = buf[0] & !(1 << 2);
            self.write_enable().unwrap();
            self.write_enable_status_register().unwrap();
            self.write_status_register(
                &[byte_wps_clear],
                RegisterInstruction::WriteStatusRegister3,
            )
            .unwrap();
            while self.is_busy() {}
        }
    }

    /// If the least significant bit (LSB) is 1, the corresponding block/sector is locked; if LSB=0, the
    /// corresponding block/sector is unlocked, Erase/Program operation can be performed.
    pub fn is_lock(&mut self, address: u32) -> bool {
        let mut buf: [u8; 1] = [0; 1];
        self.read_lock(address, &mut buf).unwrap();
        self.is_bit_set(buf[0], 0)
    }

    /// This function checks if the flash is working in 3 byte mode and modify the corresponding
    /// extended address register, that the flash is read/write in lower or upper memory array.
    /// The Extended Address Register is used only when the device is operating in the 3-Byte
    /// Address Mode.
    /// - The lower 128Mb memory array (00000000h – 00FFFFFFh) is selected when 0b00000000,
    ///   all instructions with 3-Byte addresses will be executed within that region.
    /// - When 0b00000001, the upper 128Mb memory array (01000000h – 01FFFFFFh) will be selected.
    pub(crate) fn check_3_byte_mode(&mut self, address: u32) -> Result<(), Error<QspiError>> {
        if self.byte_mode == ByteMode::Addr24Bit {
            const LOWER_ADDRESS_MAX: u32 = 0x00FFFFFF;
            if address > LOWER_ADDRESS_MAX {
                let upper: u8 = 1;
                self.write_enable().map_err(Error::Qspi)?;
                self.write_extended_address_register(&[upper])
                    .map_err(Error::Qspi)?;
            } else {
                let lower: u8 = 0;
                self.write_enable().map_err(Error::Qspi)?;
                self.write_extended_address_register(&[lower])
                    .map_err(Error::Qspi)?;
            }
        }

        Ok(())
    }
}
